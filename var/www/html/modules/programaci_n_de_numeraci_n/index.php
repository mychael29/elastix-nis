<?php
/* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 0.5                                                  |
  | http://www.elastix.com                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
*/

require_once "libs/paloSantoForm.class.php";
//require_once "libs/paloSantoACLclass.php";
require_once "modules/agent_console/libs/elastix2.lib.php";
require_once "libs/misc.lib.php";

function _moduleContent(&$smarty,$module_name)
{

    include_once "modules/$module_name/configs/config.php";
    require_once "modules/$module_name/libs/paloSantoUploadFile.class.php";

    // Obtengo la ruta del template a utilizar para generar el filtro.
    $base_dir = dirname($_SERVER['SCRIPT_FILENAME']);
    $templates_dir = (isset($config['templates_dir']))?$config['templates_dir']:'themes';
    $local_templates_dir = "$base_dir/modules/$module_name/".$templates_dir.'/'.$arrConfig['theme'];

    $relative_dir_rich_text = "modules/$module_name/".$templates_dir.'/'.$arrConfig['theme'];
    $smarty->assign("relative_dir_rich_text", $relative_dir_rich_text);

    load_language_module($module_name);

    $pDB = new paloDB($arrConfig['cadena_dsn']);
    //error_reporting (0);
    // Mostrar pantalla correspondiente
    $sAction = 'list_numeros';
    if (isset($_REQUEST['action'])) $sAction = $_REQUEST['action'];
    //echo $sAction;
    //exit();}
    if($sAction=='upload_numeros'||isset($_POST['cargar_datos'])||(isset($_GET['action']) && $_GET['action'] == 'csvdownload')){
        return uploadNumeroCSV($pDB, $smarty, $module_name, $local_templates_dir);
    }else{
        switch ($sAction) {
            //case 'upload_numeros':
                //return uploadNumeroCSV($pDB, $smarty, $module_name, $local_templates_dir);
            case 'new_numero':
                return newNumero($pDB, $smarty, $module_name, $local_templates_dir);
            case 'edit_numero':
                return editNumeroView($pDB, $smarty, $module_name, $local_templates_dir);
            case 'delete_numero':
                return deleteNumero($pDB, $smarty, $module_name, $local_templates_dir);
            case 'list_numeros':
                return listNumero($pDB, $smarty, $module_name, $local_templates_dir);
            default:
                return listNumero($pDB, $smarty, $module_name, $local_templates_dir);
        }
    }



}

function deleteNumero($pDB, $smarty, $module_name, $local_templates_dir){
    $oNumeros = new Numeros($pDB);
    $oNumeros->deleteNumeroDB($_GET['telefono']);
    //header("Location:?menu=numero&amp;action=list_numeros");
}

function uploadNumeroCSV($pDB, $smarty, $module_name, $local_templates_dir){

    $smarty->assign('FRAMEWORK_TIENE_TITULO_MODULO', existeSoporteTituloFramework());
    $smarty->assign('icon', 'images/list.png');
    $smarty->assign("MODULE_NAME", $module_name);
    $smarty->assign("LABEL_MESSAGE", _tr('Select file upload'));
    $smarty->assign("Format_File", _tr('Format File'));
    $smarty->assign("File", _tr('File'));
    $smarty->assign('ETIQUETA_SUBMIT', _tr('Upload'));
    $smarty->assign('ETIQUETA_DOWNLOAD', _tr('Download contacts'));
    $smarty->assign('Format_Content', _tr('"Telefono","Fecha AAAA-MM-DD","Observaciones"'));

    $form_campos = array(
        'file'    =>    array(
            "LABEL"                  => _tr('File'),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "FILE",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "text",
            "VALIDATION_EXTRA_PARAM" => "",
        ),
    );

    $oForm = new paloForm($smarty,$form_campos);
    $fContenido = $oForm->fetchForm("$local_templates_dir/form_numero_2.tpl", _tr('Load File') ,$_POST);

    if (isset($_POST['cargar_datos'])) {
        $infoArchivo = $_FILES['fileCRM'];
        if ($infoArchivo['error'] != 0) {
            $smarty->assign("mb_title", _tr('Error'));
            $smarty->assign("mb_message", _tr('Error while loading file'));
        } else {
            $sNombreTemp = $infoArchivo['tmp_name'];
            //$pDB = new paloDB($arrConfig['cadena_dsn']);
            $oCarga = new paloSantoUploadFile($pDB);
            $sEncoding = NULL;
            $bExito = $oCarga->addCampaignNumbersFromFile($sNombreTemp, $sEncoding);
            if (!$bExito) {
                $smarty->assign("mb_title", _tr('Error'));
                $smarty->assign("mb_message", _tr('Error while loading file').': '.$oCarga->errMsg);
            } else {
                $r = $oCarga->obtenerContadores();
                $smarty->assign("mb_title", _tr('Result'));
                $smarty->assign("mb_message",
                    _tr('Inserted records').': '.$r[0].'<br/>'.
                    _tr('Updated records').': '.$r[1].'<br/>'.
                    _tr('Detected charset').': '.$sEncoding);
            }
        }
    } elseif (isset($_GET['action']) && $_GET['action'] == 'csvdownload') {
        //$pDB = new paloDB($arrConfig['cadena_dsn']);
        $oCarga = new paloSantoUploadFile($pDB);
        $r = $oCarga->leerNumeros();
        if (!is_array($r)) {
            $smarty->assign("mb_title", _tr('Error'));
            $smarty->assign("mb_message", $oCarga->errMsg);
            return $oCarga->errMsg;
        } else {
            header("Cache-Control: private");
            header("Pragma: cache");
            header('Content-Type: text/csv; charset=UTF-8; header=present');
            header("Content-disposition: attachment; filename=\"contacts.csv\"");

            $fContenido = '';
            foreach ($r as $tuplaDatos) {
                $fContenido .= join(',', array_map('csv_replace', $tuplaDatos))."\r\n";
            }
        }
    }
    return $fContenido;
}

function listNumero($pDB, $smarty, $module_name, $local_templates_dir) {
    global $arrLang;

    $oNumeros = new Numeros($pDB);
    $bElastixNuevo = method_exists('paloSantoGrid','setURL');
    // Variables iniciales para posición de grid
    $offset = 0;
    $limit = 50;
    $total = 0;


    $sbusqueda = NULL;
    if (isset($_GET['busqueda']) && $_GET['busqueda']!=''){
        $sbusqueda = $_GET['busqueda'];
    } else{
        $sbusqueda = NULL;
    }
    if (isset($_POST['busqueda']) && $_POST['busqueda']!='') {
        $sbusqueda = $_POST['busqueda'];
    }else{
        $sbusqueda = NULL;
    }
    /*
    $sDniRuc = NULL;
    if (isset($_GET['dni_ruc']) && $_GET['dni_ruc']!=''){
        $sDniRuc = $_GET['dni_ruc'];
    } else{
        $sDniRuc = NULL;
    }
    if (isset($_POST['dni_ruc']) && $_POST['dni_ruc']!='') {
        $sDniRuc = $_POST['dni_ruc'];
    }else{
        $sDniRuc = NULL;
    }*/

    $_REQUEST['busqueda'] = $sbusqueda;
    //$_REQUEST['dni_ruc'] = $sDniRuc;

    $arrFormElements = getFormNumeroList();
    $oFilterForm = new paloForm($smarty, $arrFormElements);
    $htmlFilter = $oFilterForm->fetchForm("$local_templates_dir/filter-list-numeros.tpl", "", $_REQUEST);

    $oGrid = new paloSantoGrid($smarty);
    $oGrid->enableExport();

    //$sbusqueda = NULL;
    $oGrid->showFilter($htmlFilter); // Para el filtro
    //$oGrid->showFilter($oFilterForm); // Para el filtro


    if(isset($sbusqueda)){ // &&isset($sDniRuc)
        //echo $sbusqueda;
        $oGrid->addFilterControl(_tr("Filter applied ")._tr("Celular")." = ".$sbusqueda, $_REQUEST, array('busqueda'=>"todas"),true);
    }/*elseif(isset($sbusqueda)){
        $oGrid->addFilterControl(_tr("Filter applied ")._tr("Celular")." = ".$sbusqueda, $_REQUEST, array('busqueda'=>"todas"),true);
    }elseif(isset($sDniRuc)){
        $oGrid->addFilterControl(_tr("Filter applied ")._tr("DNI/RUC")." = ".$sDniRuc, $_REQUEST, array('dni_ruc'=>"todas"),true);
    }*/



    $bExportando = $bElastixNuevo
        ? $oGrid->isExportAction()
        : ( (isset( $_GET['exportcsv'] ) && $_GET['exportcsv'] == 'yes') ||
            (isset( $_GET['exportspreadsheet'] ) && $_GET['exportspreadsheet'] == 'yes') ||
            (isset( $_GET['exportpdf'] ) && $_GET['exportpdf'] == 'yes')
          ) ;

    $arrData = array();

    if (is_array($_REQUEST)) {

        $total = $oNumeros->cantidadNumero($sbusqueda); // , $sDniRuc
        //echo $total;
        if (is_null($total)) {
            $smarty->assign("mb_title", _tr("Error when connecting to database"));
            $smarty->assign("mb_message", $oCallsDetail->errMsg);
            $total = 0;
        } else {
            // Habilitar la exportación de todo el contenido consultado
            if ($bExportando) {
                $limit = $total;

            }
            // Calcular el offset de la petición de registros
            if ($bElastixNuevo) {

                $oGrid->setLimit($limit);
                //echo "limit ".$limit;
                //exit();
                $oGrid->setTotal($total);
                $offset = $oGrid->calculateOffset();
                //echo "offset ".$offset;

            } else {

                // Si se quiere avanzar a la sgte. pagina
                if (isset($_GET['nav']) && $_GET['nav'] == "end") {
                    // Mejorar el sgte. bloque.
                    if (($total%$limit)==0) {
                        $offset = $total - $limit;
                    } else {
                        $offset = $total - $total % $limit;
                    }
                }

                // Si se quiere avanzar a la sgte. pagina
                if (isset($_GET['nav']) && $_GET['nav']=="next") {
                    $offset = $_GET['start'] + $limit - 1;
                }

                // Si se quiere retroceder
                if(isset($_GET['nav']) && $_GET['nav']=="previous") {
                    $offset = $_GET['start'] - $limit - 1;
                }
            }

            // Ejecutar la consulta de los datos en el offset indicado
            if($sbusqueda!=NULL){ // ||$sDniRuc!=NULL
                $listaNumerosCallCenter = $oNumeros->getDataNumero($sbusqueda);
                if(!isset($listaNumerosCallCenter)){
                    $sbusqueda = "Datos no encontrados";
                    $listaNumerosCallCenter = NULL;
                }
            }else{
                //$sbusqueda = "Todos";
                $listaNumerosCallCenter = $oNumeros->getNumeros($limit, $offset);
            }

            $listaNumeros = $listaNumerosCallCenter;
            if (!is_array($listaNumerosCallCenter)) {
                $smarty->assign("mb_title", _tr("Error when connecting to database"));
                $smarty->assign("mb_message", $oCallsDetail->errMsg);
                $total = 0;
            } else {
                foreach ($listaNumeros as $tuplaNumeros) {
                    $tuplaData = array(
                        //NULL,
                        htmlentities($tuplaNumeros['fecha'], ENT_COMPAT, 'UTF-8'), // cedula ruc = dni ruc
                        htmlentities($tuplaNumeros['telefono'], ENT_COMPAT, 'UTF-8'),
                        htmlentities($tuplaNumeros['observaciones'], ENT_COMPAT, 'UTF-8'),
                        crear_formato_descarga($tuplaNumeros["telefono"],$tuplaNumeros['fecha'],$tuplaNumeros['id']),
                        //"<a href='?menu=numero&amp;action=edit_numero&amp;id_numero=" . $tuplaNumeros["cedula_ruc"] . "'>["._tr("Edit")."]</a>" .
                        //"<a href='#' class='numeroe_delete' value=" . $tuplaNumeros["telefono"] . "> ["._tr("Delete")."]</a>",
                    );

                    $arrData[] = $tuplaData;
                }
            }
        }
    }

    $arrColumns = array("Fecha", _tr("Telefono"), _tr("Observaciones"), _tr("Opciones"));

    if($_GET['exportspreadsheet'] == 'yes' || $_GET['exportpdf'] == 'yes' || $_GET['exportcsv'] == 'yes'){
        $arrColumns = array_diff($arrColumns, array(_tr("Options")));;
    }



    $oGrid->addNew("?menu=$module_name&action=new_numero", _tr('Nuevo numero'), TRUE);


    //  Verificamos si el usuario logoneado es del grupo "administrador"
    global $arrConf;
    $pACL = new paloACL($arrConf['elastix_dsn']['acl']);
    $username = $_SESSION["elastix_user"];
    $arrayGroupUser = $pACL->isUserAdministratorGroup($username);
    if($pACL->isUserAdministratorGroup($username)) {
        $oGrid->addNew("?menu=$module_name&action=upload_numeros", _tr('Carga masiva - Numero'), TRUE);
    }
    //echo $username . " Validate Admin : " . $arrayGroupUser;


    if($bElastixNuevo) {

        $url = construirURL(array(
            'menu'      =>  $module_name,
            'busqueda' =>  $sbusqueda,
            //'dni_ruc' =>  $sDniRuc,
        ), array('nav', 'start'));
        $oGrid->setURL($url);
        $smarty->assign('url', $url);
        $oGrid->setData($arrData);
        $oGrid->setColumns($arrColumns);
        $oGrid->setTitle(_tr('Numero List'));
        $oGrid->pagingShow(true);
        $oGrid->setNameFile_Export(_tr("Numero List"));

        return $oGrid->fetchGrid();
     } else {

        global $arrLang;

        $url = construirURL(array(
            'menu'      =>  $module_name,
            'busqueda' =>  $sbusqueda,
            //'dni_ruc' =>  $sDniRuc,
        ), array('nav', 'start'));
        $smarty->assign('url', $url);

        function _map_name($s) { return array('name' => $s); }
        $arrGrid = array("title"    => _tr("Numero List"),
                     "url"      => $url,
                     "icon"     => "images/user.png",
                     "width"    => "99%",
                     "start"    => ($total==0) ? 0 : $offset + 1,
                     "end"      => ($offset+$limit)<=$total ? $offset+$limit : $total,
                     "total"    => $total,
                     "columns"  => array_map('_map_name', $arrColumnas),
                    );
        if (isset( $_GET['exportpdf'] ) && $_GET['exportpdf'] == 'yes' && method_exists($oGrid, 'fetchGridPDF'))
            return $oGrid->fetchGridPDF($arrGrid, $arrData);
        if (isset( $_GET['exportspreadsheet'] ) && $_GET['exportspreadsheet'] == 'yes' && method_exists($oGrid, 'fetchGridXLS'))
            return $oGrid->fetchGridXLS($arrGrid, $arrData);
        if($bExportando) {
            header("Cache-Control: private");
            header("Pragma: cache");    // Se requiere para HTTPS bajo IE6
            header('Content-disposition: inline; filename="calls_detail.csv"');
            header("Content-Type: text/csv; charset=UTF-8");
        }
        if ($bExportando)
            return $oGrid->fetchGridCSV($arrGrid, $arrData);
        $sContenido = $oGrid->fetchGrid($arrGrid, $arrData, $arrLang);
        if (strpos($sContenido, '<form') === FALSE)
            $sContenido = "<form  method=\"POST\" style=\"margin-bottom:0;\" action=\"$url\">$sContenido</form>";
        return $sContenido;
    }


}


function crear_formato_descarga($telefono, $fecha, $id){

    if($_GET['exportspreadsheet'] == 'yes' || $_GET['exportpdf'] == 'yes' || $_GET['exportcsv'] == 'yes'){
        $link = '';
        return $link;
    }
    $link = "<a href='?menu=programaci_n_de_numeraci_n&amp;action=edit_numero&amp;id_numero=" . $telefono . "&amp;id_prog=".$id."'>["._tr("Edit")."]</a>" .
    "<a href='#' class='numeroe_delete' value=" . $fecha . "> ["._tr("Delete")."]</a>";

    if(!empty($telefono))return $link;
    else return '';
}

function formEditNumero($pDB, $smarty, $module_name, $local_templates_dir, $id_numero, $id_prog) {
    // Si se ha indicado cancelar, volver a listado sin hacer nada más
    if (isset($_POST['cancel'])) {
        Header("Location: ?menu=$module_name");
        return '';
    }

    $smarty->assign('FRAMEWORK_TIENE_TITULO_MODULO', existeSoporteTituloFramework());

    $arrNumero = NULL;
    $oNumeros = new Numeros($pDB);
    if (!is_null($id_numero)) {
        $arrNumero = $oNumeros->getNumero($id_prog);
        if (!is_array($arrNumero) || count($arrNumero) == 0) {
            $smarty->assign("mb_title", 'Unable to read numero');
            $smarty->assign("mb_message", 'Cannot read numero - '.$oNumeros->errMsg);
            return '';
        }
    }


    require_once("libs/paloSantoForm.class.php");
    $arrFormElements = getFormNumero($smarty, !is_null($id_numero));

    // Valores por omisión para primera carga
    if (is_null($id_numero)) {
        // Creación de nuevo numero
        //if (!isset($_POST['cedula_ruc']))    $_POST['cedula_ruc'] = '';
        if (!isset($_POST['telefono']))    $_POST['telefono'] = '';
        if (!isset($_POST['observaciones']))    $_POST['observaciones'] = '';

    } else {
        // Modificación de numero existente
        //if (!isset($_POST['fecha']))    $_POST['fecha'] = $arrNumero['fecha'];
        if (!isset($_POST['telefono']))    $_POST['telefono'] = $arrNumero['telefono'];
        if (!isset($_POST['observaciones']))    $_POST['observaciones'] = $arrNumero['observaciones'];
    }
    $oForm = new paloForm($smarty, $arrFormElements);
    if (!is_null($id_numero)) {
        $oForm->setEditMode();
        $smarty->assign("id_numero", $id_numero);
    }

    $bDoCreate = isset($_POST['submit_save_agent']);
    $bDoUpdate = isset($_POST['submit_apply_changes']);
    if ($bDoCreate || $bDoUpdate) {
        if(!$oForm->validateForm($_POST)) {
            // Falla la validación básica del formulario
            $smarty->assign("mb_title", _tr("Validation Error"));
            $arrErrores = $oForm->arrErroresValidacion;
            $strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br>";
            foreach($arrErrores as $k=>$v) {
                $strErrorMsg .= "$k ";
            }
            $strErrorMsg .= "";
            $smarty->assign("mb_message", $strErrorMsg);
        } else {
            foreach (array('date', 'telefono', 'observaciones') as $k)
                $_POST[$k] = trim($_POST[$k]);

            /*if (!preg_match('/^[[:digit:]]+$/', $_POST['cedula_ruc'])) {
                $smarty->assign("mb_title", _tr("Validation Error"));
                $smarty->assign("mb_message", _tr("Error Numero DNI/RUC"));
            }*/if (!preg_match('/^[0-9]+$/', $_POST['telefono'])) { // /^[9]\d{8}$/
                $smarty->assign("mb_title", _tr("Validation Error"));
                $strErrorMsg = "<b>"._tr('Telefono incorrecto').":</b><br>";
                $smarty->assign("mb_message",$strErrorMsg. _tr("El teléfono tiene que ser un valor numerico"));
            }else {
                $bExito = TRUE;

                $numero = array(
                    0 => translateDate($_POST['date']),
                    1 => $_POST['telefono'],
                    2 => $_POST['observaciones'],
                );
                if ($bDoCreate) {
                    $bExito = $oNumeros->addNumero($numero);
                    if (!$bExito) $smarty->assign("mb_message",
                        ""._tr("")." ".$oNumeros->errMsg); // Error Insert Numero
                } elseif ($bDoUpdate) {
                    $bExito = $oNumeros->editNumero($numero,$id_prog);
                    if (!$bExito) $smarty->assign("mb_message",
                        ""._tr("")." ".$oNumeros->errMsg); // Error Update Numero
                }
                if ($bExito) header("Location: ?menu=$module_name");
            }

        }
    }

    $smarty->assign('icon', 'images/user.png');
    $contenidoModulo = $oForm->fetchForm(
        "$local_templates_dir/new_2.tpl",
        is_null($id_numero) ? _tr("Nuevo Número") : _tr('Edit Número'),
        $_POST);
    return $contenidoModulo;
}

function newNumero($pDB, $smarty, $module_name, $local_templates_dir)
{
    return formEditNumero($pDB, $smarty, $module_name, $local_templates_dir, NULL, NULL);
}

function editNumeroView($pDB, $smarty, $module_name, $local_templates_dir)
{
    $id_numero = NULL;
    if (isset($_GET['id_numero']))
        $id_numero = $_GET['id_numero'];
    if (isset($_POST['id_numero']))
        $id_numero = $_POST['id_numero'];
    if (is_null($id_numero)) {
        Header("Location: ?menu=$module_name");
        return '';
    } else {
        if (isset($_GET['id_prog']))
        $id_prog = $_GET['id_prog'];
        if (isset($_POST['id_prog']))
        $id_prog = $_POST['id_prog'];

        return formEditNumero($pDB, $smarty, $module_name, $local_templates_dir, $id_numero, $id_prog);
    }
}

function getFormNumero(&$smarty, $bEdit)
{
    $smarty->assign("REQUIRED_FIELD", _tr("Required field"));
    $smarty->assign("CANCEL", _tr("Cancel"));
    $smarty->assign("APPLY_CHANGES", _tr("Apply changes"));
    $smarty->assign("SAVE", _tr("Save"));
    $smarty->assign("EDIT", _tr("Edit"));
    $smarty->assign("DELETE", _tr("Delete"));
    $smarty->assign("CONFIRM_CONTINUE", _tr("Are you sure you wish to continue?"));

    $arrFormElements = array(
        "date"  => array(
            "LABEL"                  => _tr('Date'),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "DATE",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "ereg",
            "VALIDATION_EXTRA_PARAM" => "^[[:digit:]]{1,2}[[:space:]]+[[:alnum:]]{3}[[:space:]]+[[:digit:]]{4}$"),
        "telefono"   => array(
            "LABEL"                  => _tr("Telefono"),
            "EDITABLE"               => "yes",
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "TEXT",
            'EDITABLE'              => $bEdit ? 'no' : 'yes',
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "numeric",
            "VALIDATION_EXTRA_PARAM" => ""),
        "observaciones"   => array(
            "LABEL"                  => _tr("Observaciones"),
            "EDITABLE"               => "yes",
            "REQUIRED"               => "no",
            "INPUT_TYPE"             => "TEXT",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "text",
            "VALIDATION_EXTRA_PARAM" => ""),
    );
    return $arrFormElements;
}
function getFormNumeroList()
{
    $arrFormElements = array(

        'busqueda'     =>  array(
            'LABEL'                     =>  _tr('Telefono celular'),
            'REQUIRED'                  =>  'no',
            'INPUT_TYPE'                =>  'TEXT',
            'INPUT_EXTRA_PARAM'         =>  '',
            'VALIDATION_TYPE'           =>  'ereg',
            'VALIDATION_EXTRA_PARAM'    =>  '^[[:digit:]]+$',
        ),
        /*
        'dni_ruc'     =>  array(
            'LABEL'                     =>  _tr('DNI/RUC'),
            'REQUIRED'                  =>  'no',
            'INPUT_TYPE'                =>  'TEXT',
            'INPUT_EXTRA_PARAM'         =>  '',
            'VALIDATION_TYPE'           =>  'ereg',
            'VALIDATION_EXTRA_PARAM'    =>  '^[[:digit:]]+$',
        ),*/
    );
    return $arrFormElements;
}

function csv_replace($s)
{
    return ($s == '') ? '""' : '"'.str_replace('"',"'", $s).'"';
}

?>
