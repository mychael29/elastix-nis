<?php
/* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 0.5                                                  |
  | http://www.elastix.com                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
  $Id: index.php,v 1.1.1.1 2007/07/06 21:31:21 gcarrillo Exp $ */

require_once "libs/paloSantoGrid.class.php";
require_once "libs/paloSantoDB.class.php";
require_once "libs/paloSantoForm.class.php";
require_once "libs/paloSantoConfig.class.php";
require_once "libs/paloSantoQueue.class.php";
require_once "libs/misc.lib.php";
require_once "modules/agent_console/libs/elastix2.lib.php";

require_once "modules/agent_console/libs/JSON.php";
require_once "modules/agent_console/libs/paloSantoConsola.class.php";

function _moduleContent(&$smarty, $module_name)
{
    //include module files
    include_once "modules/$module_name/configs/default.conf.php";
    include_once "modules/$module_name/libs/paloSantoCallsEntrante.class.php";
    global $arrConf;

    load_language_module($module_name);

    //folder path for custom templates
    $base_dir = dirname($_SERVER['SCRIPT_FILENAME']);
    $templates_dir = (isset($arrConfig['templates_dir']))?$arrConfig['templates_dir']:'themes';
    $local_templates_dir = "$base_dir/modules/$module_name/".$templates_dir.'/'.$arrConf['theme'];

    $pDB = new paloDB($cadena_dsn);
    switch (getParameter('action')) {
    case 'download':
        return downloadRecording($smarty, $module_name, $pDB);
    default:
        return reportCallsDetail($smarty, $module_name, $pDB, $local_templates_dir,$pDB2);
    }
}

function reportCallsDetail($smarty, $module_name, $pDB, $local_templates_dir,$pDB2)
{
    // Cadenas estáticas de Smarty
    $smarty->assign(array(
        "Filter"    =>  _tr('Filter'),
        "SHOW"      =>  _tr("Show"),
        'INCOMING_CAMPAIGN' =>  0,
        'OUTGOING_CAMPAIGN' =>  0,
    ));
    $oPaloConsola = new PaloSantoConsola();
    $bElastixNuevo = method_exists('paloSantoGrid','setURL');

    // Variables iniciales para posición de grid
    $offset = 0;
    $limit = 20;
    $total = 0;

    // Para poder consultar los agentes de campañas
    $oCallsDetail = new paloSantoCallsEntrante($pDB);
    $listaAgentes = $oCallsDetail->getAgents(); // OJO: Falta filtrar solo para agentes activos
    
    // Combo de agentes a partir de lista de agentes
    $comboAgentes = array('' => '('._tr('All Agents').')');
    foreach ($listaAgentes as $tuplaAgente) {
        if (!isset($comboAgentes[$tuplaAgente['number']])) {
            $sDesc = $tuplaAgente['number'].' - '.$tuplaAgente['name'];
            if ($tuplaAgente['estatus'] != 'A') $sDesc .= ' ('.$tuplaAgente['estatus'].')';
            $comboAgentes[$tuplaAgente['number']] = $sDesc;
        }
    }

    // Para poder consultar campañas entrantes y salientes
    $campaignID = $oCallsDetail->getCampaigns('incoming');

    $urlVars = array('menu' => $module_name);
    $arrFormElements = createFieldFilter($comboAgentes, $campaignID);
    $oFilterForm = new paloForm($smarty, $arrFormElements);

    // Validar y aplicar las variables de filtro
    $paramLista = NULL;
    $paramFiltro = array();
    foreach (array('date_start', 'date_end', 'agent','phone') as $k)
        $paramFiltro[$k] = getParameter($k);
    if (!isset($paramFiltro['date_start'])) $paramFiltro['date_start'] = date("d M Y");
    if (!isset($paramFiltro['date_end'])) $paramFiltro['date_end'] = date("d M Y");
    if (!$oFilterForm->validateForm($paramFiltro)) {
        // Hay un error al validar las variables del filtro
        $smarty->assign("mb_title", _tr("Validation Error"));
        $arrErrores = $oFilterForm->arrErroresValidacion;
        $strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br>";
        $strErrorMsg = implode(', ', array_keys($arrErrores));
        $smarty->assign("mb_message", $strErrorMsg);
    } else {
        $urlVars = array_merge($urlVars, $paramFiltro);
        $paramLista = $paramFiltro;
        $paramLista['date_start'] = translateDate($paramFiltro['date_start']) . " 00:00:00";
        $paramLista['date_end'] = translateDate($paramFiltro['date_end']) . " 23:59:59";
    }
    $htmlFilter = $oFilterForm->fetchForm("$local_templates_dir/filter.tpl", "", $paramFiltro);

    // Inicio de objeto grilla y asignación de filtro
    $oGrid = new paloSantoGrid($smarty);
    $oGrid->enableExport();   // enable export.
    $oGrid->showFilter($htmlFilter);
    $bExportando = $bElastixNuevo
        ? $oGrid->isExportAction()
        : ( (isset( $_GET['exportcsv'] ) && $_GET['exportcsv'] == 'yes') ||
            (isset( $_GET['exportspreadsheet'] ) && $_GET['exportspreadsheet'] == 'yes') ||
            (isset( $_GET['exportpdf'] ) && $_GET['exportpdf'] == 'yes')
          ) ;
          
    // Ejecutar la consulta con las variables ya validadas
    $arrData = array();
    $total = 0;
    if (is_array($paramLista)) {
        $total = $oCallsDetail->contarDetalleFormEntrantes();
        if (is_null($total)) {
            $smarty->assign("mb_title", _tr("Error when connecting to database"));
            $smarty->assign("mb_message", $oCallsDetail->errMsg);
            $total = 0;
        } else {
            // Habilitar la exportación de todo el contenido consultado
            if ($bExportando) $limit = $total;
            // Calcular el offset de la petición de registros
            if ($bElastixNuevo) {
                $oGrid->setLimit($limit);
                $oGrid->setTotal($total);
                $offset = $oGrid->calculateOffset();
            } else {
                // Si se quiere avanzar a la sgte. pagina
                if (isset($_GET['nav']) && $_GET['nav'] == "end") {
                    // Mejorar el sgte. bloque.
                    if (($total%$limit)==0) {
                        $offset = $total - $limit;
                    } else {
                        $offset = $total - $total % $limit;
                    }
                }

                // Si se quiere avanzar a la sgte. pagina
                if (isset($_GET['nav']) && $_GET['nav']=="next") {
                    $offset = $_GET['start'] + $limit - 1;
                }

                // Si se quiere retroceder
                if(isset($_GET['nav']) && $_GET['nav']=="previous") {
                    $offset = $_GET['start'] - $limit - 1;
                }
            }

            // Ejecutar la consulta de los datos en el offset indicado
            $recordset = $oCallsDetail->getReportForm($limit, $offset, $id_agent, $date_start, $date_end);
            if (!is_array($recordset)) {
                $smarty->assign("mb_title", _tr("Error when connecting to database"));
                $smarty->assign("mb_message", $oCallsDetail->errMsg);
                $total = 0;
            } else {
                /*Generacion de Columnas*/
                $arrColumnas = array(_tr("Id Campaña"),_tr("Fecha y Hora"), _tr("Número"), _tr("Agente"));
                $i=0;
                foreach($recordset as $result){
                    $jsonData = json_decode($result[5],1);

                    foreach ($jsonData as $tuplaFormulario) {
                        if(!in_array($tuplaFormulario['label'],$col_array)){
                            if(in_array($tuplaFormulario['label'],$col_array_ignore)) continue;
                            if($tuplaFormulario['label'] == 'observaciones')$col_array[100] = $tuplaFormulario['label'];
                            else $col_array[$i] = $tuplaFormulario['label'];
            
                            $i++;
                        }
                    }
                    /*
                    foreach ($jsonData as $key => $value) {
                        if(!in_array($key,$col_array)){
                            if(in_array($key,$col_array_ignore)) continue;
                            if($key == 'observaciones')$col_array[100] = $key;
                            else $col_array[$i] = $key;
            
                            $i++;
                        }
                    }*/
                }
                ksort($col_array);
                foreach($col_array as $key){
                    $arrColumnas[] = _tr(ucwords(str_replace("_"," ",$key)));
                }
                $arraColumns = array_values($arrColumnas);
                //$oGrid->setColumns($arrColumnas);

                /*Datos de reporte formulario*/
                if(is_array($recordset) && $total>0){
                    foreach($recordset as $dataForm){ 
                        $arrTmp = [];
                        $arrTmp[0] = $dataForm['campania_id'];
                        $arrTmp[1] = date("d/m/Y h:i:s a",strtotime($value['fecha'])+5*3600); 
                        $arrTmp[2] = $dataForm['nombre_cliente'];
                        $arrTmp[3] = $dataForm['phone'];
                        $arrTmp[4] = $dataForm['agente'];
                        $json_data = json_decode($dataForm['data'],1);
                        foreach($col_array as $columna){
                            if(!empty($json_data[$columna]))$arrTmp[] = $json_data[$columna];
                            else $arrTmp[] = "-----";
                        }
                        $arrData[] = $arrTmp;
                    }
                }
                /*
                if(is_array($recordset) && $total>0){
                    foreach($recordset as $key => $value){ 
                        $arrTmp = [];
                        $arrTmp[0] = $value['0'];
                        $arrTmp[1] = date("d/m/Y h:i:s a",strtotime($value['1'])+5*3600); 
                        $arrTmp[2] = $value['2'];
                        $arrTmp[3] = $value['3'];
                        $arrTmp[4] = $value['4'];
                        $json_data = json_decode($value['5'],1);
                        foreach($col_array as $columna){
                            if(!empty($json_data[$columna]))$arrTmp[] = $json_data[$columna];
                            else $arrTmp[] = "-----";
                        }
                        $arrData[] = $arrTmp;
                    }
                }*/
            }
        }
    }

    if($bElastixNuevo) {
        $oGrid->setURL(construirURL($urlVars, array("nav", "start")));
        $oGrid->setData($arrData);
        $oGrid->setColumns($arrColumnas);
        $oGrid->setTitle(_tr("Reporte Formulario de Llamadas Entrantes"));
        $oGrid->pagingShow(true);
        $oGrid->setNameFile_Export(_tr("Reporte Formulario Entrantes"));
        return $oGrid->fetchGrid();
     } else {
        global $arrLang;

        $url = construirURL($urlVars, array("nav", "start"));
        function _map_name($s) { return array('name' => $s); }
        $arrGrid = array("title"    => _tr("Calls Detail"),
                     "url"      => $url,
                     "icon"     => "images/user.png",
                     "width"    => "99%",
                     "start"    => ($total==0) ? 0 : $offset + 1,
                     "end"      => ($offset+$limit)<=$total ? $offset+$limit : $total,
                     "total"    => $total,
                     "columns"  => array_map('_map_name', $arrColumnas),
                    );
        if (isset( $_GET['exportpdf'] ) && $_GET['exportpdf'] == 'yes' && method_exists($oGrid, 'fetchGridPDF'))
            return $oGrid->fetchGridPDF($arrGrid, $arrData);
        if (isset( $_GET['exportspreadsheet'] ) && $_GET['exportspreadsheet'] == 'yes' && method_exists($oGrid, 'fetchGridXLS'))
            return $oGrid->fetchGridXLS($arrGrid, $arrData);
        if($bExportando) {
            header("Cache-Control: private");
            header("Pragma: cache");    // Se requiere para HTTPS bajo IE6
            header('Content-disposition: inline; filename="calls_detail_form_entrante.csv"');
            header("Content-Type: text/csv; charset=UTF-8");
        }
        if ($bExportando)
            return $oGrid->fetchGridCSV($arrGrid, $arrData);
        $sContenido = $oGrid->fetchGrid($arrGrid, $arrData, $arrLang);
        if (strpos($sContenido, '<form') === FALSE)
            $sContenido = "<form  method=\"POST\" style=\"margin-bottom:0;\" action=\"$url\">$sContenido</form>";
        return $sContenido;
    }
}

function createFieldFilter($comboAgentes, $campaignIn)
{
    // Combo de campañas entrantes
    $comboCampaignIn = array('' => '('._tr('All Incoming Campaigns').')');
    foreach ($campaignIn as $tuplaCampania) {
        $comboCampaignIn[$tuplaCampania['id']] =
            (($tuplaCampania['estatus'] != 'A') ? '('.$tuplaCampania['estatus'].') ' : '').
            $tuplaCampania['name'];
    }

    $arrFormElements = array(
        "date_start"  => array(
            "LABEL"                  => _tr('Start Date'),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "DATE",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "ereg",
            "VALIDATION_EXTRA_PARAM" => "^[[:digit:]]{1,2}[[:space:]]+[[:alnum:]]{3}[[:space:]]+[[:digit:]]{4}$"),
        "date_end"    => array(
            "LABEL"                  => _tr("End Date"),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "DATE",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "ereg",
            "VALIDATION_EXTRA_PARAM" => "^[[:digit:]]{1,2}[[:space:]]+[[:alnum:]]{3}[[:space:]]+[[:digit:]]{4}$"),
        'agent'     =>  array(
            'LABEL'                     =>  _tr('No.Agent'),
            'REQUIRED'                  =>  'no',
            'INPUT_TYPE'                =>  'SELECT',
            'INPUT_EXTRA_PARAM'         =>  $comboAgentes,
            'VALIDATION_TYPE'           =>  'ereg',
            'VALIDATION_EXTRA_PARAM'    =>  '^[[:digit:]]+$',
            'ONCHANGE'                  =>  'submit();',
        ),
        'phone'     =>  array(
            'LABEL'                     =>  _tr('Destino'),
            'REQUIRED'                  =>  'no',
            'INPUT_TYPE'                =>  'TEXT',
            'INPUT_EXTRA_PARAM'         =>  '',
            'VALIDATION_TYPE'           =>  'ereg',
            'VALIDATION_EXTRA_PARAM'    =>  '^[[:digit:]]+$',
        ),
        'id_campaign_in'    => array(
            'LABEL'                     =>  _tr('Incoming Campaign'),
            'REQUIRED'                  =>  'no',
            'INPUT_TYPE'                =>  'SELECT',
            'INPUT_EXTRA_PARAM'         =>  $comboCampaignIn,
            'VALIDATION_TYPE'           =>  'ereg',
            'VALIDATION_EXTRA_PARAM'    =>  '^[[:digit:]]+$',
            'ONCHANGE'                  =>  'submit();',
        ),
    );
    return $arrFormElements;
}


?>
