<?php
/* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 0.5                                                  |
  | http://www.elastix.com                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
  $Id: new_campaign.php $ */

require_once "libs/paloSantoForm.class.php";
require_once "libs/misc.lib.php";
require_once "libs/paloSantoConfig.class.php";
require_once "libs/paloSantoGrid.class.php";

if (!function_exists('_tr')) {
    function _tr($s)
    {
        global $arrLang;
        return isset($arrLang[$s]) ? $arrLang[$s] : $s;
    }
}
if (!function_exists('load_language_module')) {
    function load_language_module($module_id, $ruta_base='')
    {
        $lang = get_language($ruta_base);
        include_once $ruta_base."modules/$module_id/lang/en.lang";
        $lang_file_module = $ruta_base."modules/$module_id/lang/$lang.lang";
        if ($lang != 'en' && file_exists("$lang_file_module")) {
            $arrLangEN = $arrLangModule;
            include_once "$lang_file_module";
            $arrLangModule = array_merge($arrLangEN, $arrLangModule);
        }

        global $arrLang;
        global $arrLangModule;
        $arrLang = array_merge($arrLang,$arrLangModule);
    }
}
function _moduleContent(&$smarty, $module_name)
{
    require_once "modules/$module_name/libs/paloSantoCallsHour.class.php";

    #incluir el archivo de idioma de acuerdo al que este seleccionado
    #si el archivo de idioma no existe incluir el idioma por defecto
    $lang=get_language();
    $script_dir=dirname($_SERVER['SCRIPT_FILENAME']);

    load_language_module($module_name);
    
    //include module files
    include_once "modules/$module_name/configs/default.conf.php";
    global $arrConf;

    // Se fusiona la configuración del módulo con la configuración global
    $arrConf = array_merge($arrConf, $arrConfModule);

    //folder path for custom templates
    $base_dir = dirname($_SERVER['SCRIPT_FILENAME']);
    $templates_dir = (isset($arrConf['templates_dir']))?$arrConf['templates_dir']:'themes';
    $local_templates_dir = "$base_dir/modules/$module_name/".$templates_dir.'/'.$arrConf['theme'];

    // Conexión a la base de datos CallCenter
    $pDB = new paloDB($arrConf['cadena_dsn']);

    // Mostrar pantalla correspondiente
    $contenidoModulo = '';
    $sAction = 'list_campaign';

    if (isset($_GET['action'])) $sAction = $_GET['action'];
    switch ($sAction) {
    case 'graph_histogram':
        $contenidoModulo = graphHistogram($pDB, $smarty, $module_name, $local_templates_dir);
    break;
    case 'list_histogram':
    default:
        $contenidoModulo = listHistogram($pDB, $smarty, $module_name, $local_templates_dir);
        break;
    }

    return $contenidoModulo;
}

function sumar($a, $b) { return $a + $b; }

function listHistogram($pDB, $smarty, $module_name, $local_templates_dir)
{
    global $arrLang;

    $sEstadoLlamada = 'T';
    
    // Rango de fechas
    $sFechaInicial = $sFechaFinal = date('Y-m-d');
    if (isset($_GET['fecha_ini'])) $sFechaInicial = date('Y-m-d', strtotime($_GET['fecha_ini']));
    if (isset($_POST['fecha_ini'])) $sFechaInicial = date('Y-m-d', strtotime($_POST['fecha_ini']));
    if (isset($_GET['fecha_fin'])) $sFechaFinal = date('Y-m-d', strtotime($_GET['fecha_fin']));
    if (isset($_POST['fecha_fin'])) $sFechaFinal = date('Y-m-d', strtotime($_POST['fecha_fin']));
    $_POST['fecha_ini'] = date('d M Y', strtotime($sFechaInicial));
    $_POST['fecha_fin'] = date('d M Y', strtotime($sFechaFinal));
    $smarty->assign('FECHA_INI', $sFechaInicial);
    $smarty->assign('FECHA_FIN', $sFechaFinal);

    // Recuperar la lista de llamadas
    $oCalls = new paloSantoCallsHour($pDB);

    $comboExtension = array();
    $comboExtension = $oCalls->getSrcExtension();

    /*
    $sExtensionLlamada = NULL;
    if (isset($_GET['extension']) && $_GET['extension']!='Todas'){
        $sExtensionLlamada = $_GET['extension'];
    } else{
        $sExtensionLlamada = NULL;
    }     
    if (isset($_POST['extension']) && $_POST['extension']!='Todas') {
        $sExtensionLlamada = $_POST['extension'];
    }else{
        $sExtensionLlamada = NULL;
    }*/
    //$smarty->assign('EXTENSION', $sExtensionLlamada);
    $smarty->assign('EXTENSION', $sExtensionLlamada);

    /*
    $sEfectividad = NULL;
    if (isset($_GET['efectividad']) && $_GET['efectividad']!=''){
        $sEfectividad = $_GET['efectividad'];
    } else{
        $sEfectividad = NULL;
    } 
    if (isset($_POST['efectividad']) && $_POST['efectividad']!='') {
        $sEfectividad = $_POST['efectividad'];
    }else{
        $sEfectividad = NULL;
    }*/

    $sExtensionLlamada = NULL;
    $sEfectividad = NULL;
    if (isset($_GET['extension'])) $sExtensionLlamada = $_GET['extension'];
    if (isset($_POST['extension'])) $sExtensionLlamada = $_POST['extension'];
    if (isset($_GET['efectividad'])) $sEfectividad = $_GET['efectividad'];
    if (isset($_POST['efectividad'])) $sEfectividad = $_POST['efectividad'];

    $arrCalls = $oCalls->getCalls($sEstadoLlamada, $sFechaInicial, $sFechaFinal, $sExtensionLlamada);
    $arrCalls_contestados = $oCalls->getCalls('E', $sFechaInicial, $sFechaFinal, $sExtensionLlamada); // E : ANSWERED

    $arrCalls_no_contestados = $oCalls->getCalls('N', $sFechaInicial, $sFechaFinal, $sExtensionLlamada);

    //$arrCalls_efectividad = $oCalls->getCallsEfectividad('E', $sFechaInicial, $sFechaFinal, $sExtensionLlamada,$sEfectividad); 

    
    if($sEfectividad!=NULL){
        $arrCalls_efectividad = $oCalls->getCallsEfectividad('E', $sFechaInicial, $sFechaFinal, $sExtensionLlamada,$sEfectividad); 
    }else{
        $arrCalls_efectividad=NULL;
    }
    $smarty->assign('EFECTIVIDAD', $sEfectividad);



    // TODO: manejar error al obtener llamadas
    if (!is_array($arrCalls)) {
        $smarty->assign("mb_title", _tr("Validation Error"));
        $smarty->assign("mb_message", $oCalls->errMsg);
        $arrCalls = array();
    }
    if (!is_array($arrCalls_contestados)) {
        $smarty->assign("mb_title", _tr("Validation Error"));
        $smarty->assign("mb_message", $oCalls->errMsg);
        $arrCalls_contestados = array();
    }

    if (!is_array($arrCalls_efectividad)) {
        $smarty->assign("mb_title", _tr("Validation Error"));
        $smarty->assign("mb_message", $oCalls->errMsg);
        $arrCalls_efectividad = array();
    }

    $url = construirURL(array(
        'menu'      =>  $module_name,
        'fecha_ini' =>  $sFechaInicial,
        'fecha_fin' =>  $sFechaFinal,
        'extension' =>  $sExtensionLlamada,
        'efectividad' =>  $sEfectividad,
    ), array('nav', 'start'));
    $smarty->assign('url', $url);

    // Construir el arreglo como debe mostrarse en la tabla desglose
    $arrData = array();
    for ($i = 0; $i < 24; $i++) {
        $arrData[$i] = array(sprintf('%02d:00', $i));
    }
    $arrData[24] = array(_tr('Total Calls'));
    $arrCols = array(
        0   =>  array('name' => _tr('Hour')),
    );

    // Se movio mas arriba este codigo
    $smarty->assign('MODULE_NAME', $module_name);
    $smarty->assign('LABEL_FIND', _tr('Find'));
    $formFilter = getFormFilter($comboExtension);
    $oForm = new paloForm($smarty, $formFilter);


    $arrTodos = array_fill(0, 24, 0);
    $arrTodos_contestados = array_fill(0, 24, 0);
    $arrTodos_no_contestados = array_fill(0, 24, 0);

    $arrTodos_efectividad = array_fill(0, 24, 0);
    // Llamadas contestadas
    $arrCols[] = array('name' => _tr('Contestadas'));


    if(count($arrCalls_contestados)!=0){
       
        foreach ($arrCalls_contestados as $sQueue => $hist){
            $iTotalCola = 0;
            foreach ($hist as $i => $iNumCalls) {
                
                $arrData[$i][] = $iNumCalls;
                $arrTodos_contestados[$i] += $iNumCalls;
                $iTotalCola += $iNumCalls;
            }
            $arrData[24][] = $iTotalCola;
        }  
    
    
    }else{
        for($i = 0; $i<24; $i++){
            $arrData[$i][] = 0;
        }
        $arrData[24][] = 0;
    }


    // Llamadas no contestadas
    if(count($arrCalls_no_contestados)!=0){
        foreach ($arrCalls_no_contestados as $sQueue => $hist)   {
            $iTotalCola = 0;
            foreach ($hist as $i => $iNumCalls) {
                $arrTodos_no_contestados[$i] += $iNumCalls;
            }
        } 

    }else{
       
    }

    // Llamadas efectivas
    $arrCols[] = array('name' => _tr('Efectividad'));

    
    if(count($arrCalls_efectividad)!=0){ // is_int, verifica si la variable es un entero
        foreach ($arrCalls_efectividad as $sQueue => $hist)  {
            $iTotalCola = 0;
            foreach ($hist as $i => $iNumCalls) {

                $arrData[$i][] = $iNumCalls;
                $arrTodos_efectividad[$i] += $iNumCalls;
                $iTotalCola += $iNumCalls;
            }
            $arrData[24][] = $iTotalCola;
        }  


    }else{
        
        for($i = 0; $i<24; $i++){
            $arrData[$i][] = 0; 
        }
        $arrData[24][] = 0;
    }
    
    // Todas las llamadas realizadas
    $arrCols[] = array('name' => _tr('Todas las llamadas'));
    $detenerciclo = true;
    if(count($arrCalls)!=0){
        foreach ($arrCalls as $sQueue => $hist)    

        if($detenerciclo){
            $iTotalCola = 0;
            foreach ($hist as $i => $iNumCalls) {
                $arrData[$i][] = $arrTodos_no_contestados[$i] + $arrTodos_contestados[$i];
                $arrTodos[$i] += $iNumCalls;
                $iTotalCola += $arrTodos_no_contestados[$i] + $arrTodos_contestados[$i];
            }
            $arrData[24][] = $iTotalCola;
            $detenerciclo = false;
        }

    }else{
        for($i = 0; $i<24; $i++){
            $arrData[$i][] = 0; 
        }
        $arrData[24][] = 0;
    }

  

    //Llenamos las cabeceras
    $arrGrid = array("title"    => _tr("Calls per hour"),
        "url"      => $url,
        "icon"     => "images/list.png",
        "width"    => "99%",
        "start"    => 0,
        "end"      => 0,
        "total"    => 0,
        "columns"  => $arrCols);
    $oGrid = new paloSantoGrid($smarty);
    $oGrid->showFilter(
        $oForm->fetchForm(
            "$local_templates_dir/filter-calls.tpl", 
            NULL,
            $_POST)
    );
    $oGrid->enableExport();
    if (isset($_GET['exportcsv']) && $_GET['exportcsv'] == 'yes') {
        $fechaActual = date("Y-m-d");
        header("Cache-Control: private");
        header("Pragma: cache");
        header('Content-Type: text/csv; charset=UTF-8; header=present');
        $title = "\"calls-per-hour-".$fechaActual.".csv\"";
        header("Content-disposition: attachment; filename={$title}");
        return $oGrid->fetchGridCSV($arrGrid, $arrData);
    } else {
        $bExportando =
              ( (isset( $_GET['exportcsv'] ) && $_GET['exportcsv'] == 'yes') || 
                (isset( $_GET['exportspreadsheet'] ) && $_GET['exportspreadsheet'] == 'yes') || 
                (isset( $_GET['exportpdf'] ) && $_GET['exportpdf'] == 'yes')
              ) ;
        $sContenido = $oGrid->fetchGrid($arrGrid, $arrData, $arrLang);
        if (!$bExportando) {
            if (strpos($sContenido, '<form') === FALSE)
                $sContenido = "<form  method=\"POST\" style=\"margin-bottom:0;\" action=\"$url\">$sContenido</form>";
        }
        return $sContenido;
    }
}

function getFormFilter($arrComboExtension)
{
    $formCampos = array(
        "fecha_ini"       => array(
            "LABEL"                  => _tr("Date Init"),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "DATE",
            "INPUT_EXTRA_PARAM"      => array("TIME" => false, "FORMAT" => "%d %b %Y"),
            "VALIDATION_TYPE"        => 'ereg',
            "VALIDATION_EXTRA_PARAM" => '^[[:digit:]]{2}[[:space:]]+[[:alpha:]]{3}[[:space:]]+[[:digit:]]{4}$'
        ),
        "fecha_fin"       => array(
            "LABEL"                  => _tr("Date End"),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "DATE",
            "INPUT_EXTRA_PARAM"      => array("TIME" => false, "FORMAT" => "%d %b %Y"),
            "VALIDATION_TYPE"        => 'ereg',
            "VALIDATION_EXTRA_PARAM" => '^[[:digit:]]{2}[[:space:]]+[[:alpha:]]{3}[[:space:]]+[[:digit:]]{4}$'
        ),
        "extension" => array(
            "LABEL"                  => _tr("Extension"),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "SELECT",
            "INPUT_EXTRA_PARAM"      => $arrComboExtension,
            "VALIDATION_TYPE"        => "numeric",
            "VALIDATION_EXTRA_PARAM" => ""
        ),
        "efectividad" => array(
            "LABEL"                  => _tr("Efectividad"),
            "REQUIRED"               => "yes",
            "INPUT_TYPE"             => "TEXT",
            "INPUT_EXTRA_PARAM"      => "",
            "VALIDATION_TYPE"        => "ereg",
            "VALIDATION_EXTRA_PARAM" => '^[[:digit:]]{9}$'
        ),
        
    );

    return $formCampos;
}


function graphHistogram($pDB, $smarty, $module_name, $local_templates_dir)
{
    require_once 'libs/paloSantoGraphImage.lib.php';

    // Tipo de llamada Entrante o Saliente
    
    //$sTipoLlamada = $_GET['tipo'];
    //if (!in_array($sTipoLlamada, array('E', 'S'))) return '';

    // Fechas inicial y final del rango
    if (!isset($_GET['fecha_ini'])) return '';
    if (!isset($_GET['fecha_fin'])) return '';
    if (!isset($_GET['efectividad'])) return '';
    $sFechaInicial = $_GET['fecha_ini']; $sFechaFinal = $_GET['fecha_fin'];
    $sExtensionLlamada = $_GET['extension'];
    $sEfectividad = $_GET['efectividad'];
    $sFormatoFecha = '^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$';
    if (!preg_match("/".$sFormatoFecha."/", $sFechaInicial)) return '';
    if (!preg_match("/".$sFormatoFecha."/", $sFechaFinal)) return '';

    
    //echo "XXX ".$sEfectividad;
    //$smarty->assign('EXTENSION', $sExtensionLlamada);

    
    // Recuperar la lista de llamadas
    $arrCalls = array();
    $oCalls = new paloSantoCallsHour($pDB);
    
    $arrCalls['todas'] = $oCalls->getCalls('T', $sFechaInicial, $sFechaFinal, $sExtensionLlamada);
    if (!is_array($arrCalls['todas'])) return $oCalls->errMsg;
    $arrCalls['contestadas'] = $oCalls->getCalls('E', $sFechaInicial, $sFechaFinal, $sExtensionLlamada);
    if (!is_array($arrCalls['contestadas'])) return $oCalls->errMsg;
    $arrCalls['no_contestadas'] = $oCalls->getCalls('N', $sFechaInicial, $sFechaFinal, $sExtensionLlamada);
    if (!is_array($arrCalls['no_contestadas'])) return $oCalls->errMsg; 

    if($sEfectividad != NULL){
        $arrCalls['efectividad'] = $oCalls->getCallsEfectividad('E', $sFechaInicial, $sFechaFinal, $sExtensionLlamada, $sEfectividad); // No se puede poner valores por filtro dentro de esta funcion
        // Se pone un valor al parametro para que no que retorne error
        if (!is_array($arrCalls['efectividad'])) return $oCalls->errMsg;
    }

    
     
    
    //$sQueue = '';
    //if (isset($_GET['extension'])) $sQueue = $_GET['extension'];

    /**
     *  Ver bien el flujo para mejorar codigo
     */
    if (!in_array($sExtensionLlamada, array_keys($arrCalls['todas']))) $sExtensionLlamada = NULL; 

    $listaVacia = array_fill(0, 24, 0);
    $graphdata = array();

    if ($sExtensionLlamada != NULL) {
        $graphdata['todas'] = $arrCalls['todas'];  // Por definición, siempre existe
        $graphdata['contestadas'] = isset($arrCalls['contestadas'])
            ? $arrCalls['contestadas']
            : $listaVacia;
        if($sEfectividad != NULL){
            $graphdata['efectividad'] = isset($arrCalls['efectividad'])
            ? $arrCalls['efectividad']
            : $listaVacia;
        }else{
            $graphdata['no_contestadas'] = isset($arrCalls['no_contestadas'])
            ? $arrCalls['no_contestadas']
            : $listaVacia;
        }
        
    } else {
        $graphdata['todas'] = $listaVacia;
        $graphdata['contestadas'] = $listaVacia;
        $graphdata['efectividad'] = $listaVacia;
        if($sEfectividad!=NULL){
            foreach (array('todas', 'contestadas', 'efectividad') as $k) {
                foreach ($arrCalls[$k] as $sQueue => $hist)
                    $graphdata[$k] = array_map('sumar', $graphdata[$k], $hist);
            }
        }else{
            foreach (array('todas', 'contestadas', 'no_contestadas') as $k) {
                foreach ($arrCalls[$k] as $sQueue => $hist)
                    $graphdata[$k] = array_map('sumar', $graphdata[$k], $hist);
            } 
        }
        
    }

    
    //exit();    

    // Labels
    $datahours = array();
    for ($i = 0; $i < 24; $i++) {
        $datahours[] = sprintf('%02d', $i);
    }

    // Setup the graph
    $graph = new Graph(800,500);
   
    $graph->SetMarginColor('white');
    $graph->SetScale("textlin");
    $graph->SetFrame(true);
    $graph->SetMargin(60,50,30,30);

    $graph->title->Set(_tr('Calls'));

    $graph->yaxis->HideZeroLabel();
    $graph->ygrid->SetFill(true,'#EFEFEF@0.5','#BBCCFF@0.5');
    $graph->xgrid->Show();

    $graph->xaxis->SetTickLabels($datahours);

    // Create the first line
    $p1 = new LinePlot($graphdata['todas']);
    $p1->SetColor("navy");
    $p1->SetLegend(_tr('All'));
    $p1->SetWeight(7);
    $graph->Add($p1);

    // Create the second line
    $p2 = new LinePlot($graphdata['contestadas']);
    $p2->SetColor("red");
    $p2->SetLegend(_tr('Contestadas'));
    $p2->SetWeight(3);
    $graph->Add($p2);

    
    // Create the third line
    if($sEfectividad!=NULL){
        $p3 = new LinePlot($graphdata['efectividad']);
        $p3->SetColor("green");
        $p3->SetLegend(_tr('Efectividad'));
        $p3->SetWeight(3);
        $graph->Add($p3);
    }
    

    $graph->legend->SetShadow('gray@0.4',5);
    $graph->legend->SetPos(0.1,0.1,'right','top');
    // Output line

    
    $graph->Stroke();
    
}

?>
