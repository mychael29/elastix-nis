$(document).ready(function() {

	$('.numeroe_delete').click(function() {
		var telefono = $(this).attr('value');
		var pregunta_borrar_cliente = "¿Estas seguro que quieres borrarlo?";
		PromptBorrar(telefono, 'delete_numero', pregunta_borrar_cliente);
	});
});

function PromptBorrar(telefono , action, pregunta_borrar_cliente) {
	var bool=confirm("Seguro de eliminar?");

	if(bool){
		$.get('index.php', {
			menu:	'numeraci_n_asignada',
			rawmode:	'yes',
			action:		action,
			telefono:	telefono
		}, function (respuesta) {
			if (respuesta.status == 'error') {
				alert(respuesta.message);
			} else {
				alert("Se ha eliminado el numero ");
				location.reload();
			}
		});
	}else{
	  alert("Cancelo la solicitud");
	}


	  /*
	//Ingresamos un mensaje a mostrar
	var validar = prompt(pregunta_borrar_cliente, "SI");
	console.log(validar)
	var validar2 = validar.toLowerCase();
	if (validar2 == "si" ){ // No funcionara en otros idiomas excepto español / english

		$.get('index.php', {
			menu:	'client',
			rawmode:	'yes',
			action:		action,
			telefono:	telefono
		}, function (respuesta) {
			if (respuesta.status == 'error') {
				alert(respuesta.message);
			} else {
				alert("Se ha eliminado el cliente ");
				location.reload();
			}
		});
	}
	//Detectamos si el usuario NO ingreso un valor
	else {
	alert("No se ha podido eliminar");
	}*/
}