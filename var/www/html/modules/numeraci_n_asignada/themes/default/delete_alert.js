$(document).ready(function() {
	console.log('hello!');
	$('a.cliente_delete').click(function() {
		var telefono = $(this).attr('value');
		var pregunta_borrar_cliente = "Are you sure you want to delete? Write DELETE to continue";
		PromptBorrar(telefono, 'delete_client', pregunta_borrar_cliente);
	});
});

function PromptBorrar(telefono , action, pregunta_borrar_cliente) {
	//Ingresamos un mensaje a mostrar
	var validar = prompt(pregunta_borrar_agente_conf, "");
	//Detectamos si el usuario ingreso un valor
	if (validar == "delete" || validar == "eliminar" ){ // No funcionara en otros idiomas excepto español / english
		alert("Se ha eliminado el cliente ");
		$.get('index.php', {
			menu:	'client',
			rawmode:	'yes',
			action:		action,
			telefono:	telefono
		}, function (respuesta) {
			if (respuesta.status == 'error') {
				alert(respuesta.message);
			} else {
				// location.reload();
			}
		});
	}
	//Detectamos si el usuario NO ingreso un valor
	else {
	alert("No se ha podido eliminar");
	}
}