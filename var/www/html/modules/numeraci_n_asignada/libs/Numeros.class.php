<?php

/* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 0.5                                                  |
  | http://www.elastix.com                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2007 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
*/
/*
if (file_exists("/var/lib/asterisk/agi-bin/phpagi-asmanager.php")) {
    include_once "/var/lib/asterisk/agi-bin/phpagi-asmanager.php";
} elseif (file_exists('libs/phpagi-asmanager.php')) {
	include_once 'libs/phpagi-asmanager.php';
} else {
	die('Unable to find phpagi-asmanager.php');
}*/
include_once("libs/paloSantoDB.class.php");

class Numeros
{
    var $arrAgents;
    private $_DB; // instancia de la clase paloDB
    var $errMsg;

    function Numeros(&$pDB)
    {
        // Se recibe como parámetro una referencia a una conexión paloDB
        if (is_object($pDB)) {
            $this->_DB =& $pDB;
            $this->errMsg = $this->_DB->errMsg;
        } else {
            $dsn = (string)$pDB;
            $this->_DB = new paloDB($dsn);

            if (!$this->_DB->connStatus) {
                $this->errMsg = $this->_DB->errMsg;
                // debo llenar alguna variable de error
            } else {
                // debo llenar alguna variable de error
            }
        }

        $this->arrAgents = array();
    }

    function getNumeros($limit = NULL, $offset = 0, $id=null){

        $sQuery = "SELECT * FROM numeros_asignados ORDER BY fecha DESC LIMIT $limit OFFSET $offset";
        if (!empty($limit)) {
            $sQuery . " LIMIT $limit OFFSET $offset";
            //array_push($paramSQL, $limit, $offset);
        }
        $arr_result =& $this->_DB->fetchTable($sQuery, true, null);

        if (is_array($arr_result)) {
            if (is_null($id) || count($arr_result) <= 0) {
                return $arr_result;
            } else {
                return $arr_result[0];
            }

        } else {
            $this->errMsg = 'Unable to read numero information - '.$this->_DB->errMsg;
            return NULL;
        }

    }

    function deleteNumeroDB($telefono){

        if (!preg_match('/^[[:digit:]]+$/', $telefono)) {
            $this->errMsg = '(internal) Invalid numero information';
            return FALSE;
        }

        $sPeticionSQL = "DELETE FROM numeros_asignados WHERE telefono='$telefono'";

        $result = $this->_DB->genQuery($sPeticionSQL);
        if (!$result) {
            $this->errMsg = $this->_DB->errMsg;
            $this->_DB->genQuery("ROLLBACK");
            $this->_DB->genQuery("SET AUTOCOMMIT = 1");
            return false;
        }

        return $result;
    }


    function getNumero($id){

        $sQuery = "SELECT * FROM numeros_asignados WHERE telefono = ?";
        $sParam = array($id);
        $arr_result =& $this->_DB->fetchTable($sQuery, true, $sParam);

        if (is_array($arr_result)) {
            if (is_null($id) || count($arr_result) <= 0) {
                return $arr_result;
            } else {
                return $arr_result[0];
            }

        } else {
            $this->errMsg = 'Unable to read numero information - '.$this->_DB->errMsg;
            return NULL;
        }

    }

    function getDataNumero($busqueda)
    {



        $paramQuery = array();


        if(isset($busqueda)){
            if (!preg_match('/^[0-9]+$/', $busqueda)) {   // /^[[:digit:]]+$/
                $this->errMsg = '(internal) Invalid numero information';
                return NULL;
            }
            $paramQuery[] = $busqueda;
            $sQuery = "SELECT * FROM numeros_asignados WHERE telefono = ?";
        }

        $arr_result =& $this->_DB->fetchTable($sQuery, true, $paramQuery);
        if (is_array($arr_result)) {
            if (is_null($busqueda) || count($arr_result) >= 0) {
                return $arr_result;
            } else {
                return $arr_result[0];
            }
        } else {
            $this->errMsg = 'Unable to read agent information - '.$this->_DB->errMsg;
            return NULL;
        }
    }

    function addNumero($num)
    {
        if (!is_array($num) || count($num) < 3) {
            $this->errMsg = 'Invalid numero data';
            return FALSE;
        }

        $tupla = $this->_DB->getFirstRowQuery(
            'SELECT COUNT(*) FROM numeros_asignados WHERE telefono = ?',
            FALSE, array($num[1]));
        if ($tupla[0] > 0) {
            $this->errMsg = _tr('Numero already exists');
            return FALSE;
        }

        $sPeticionSQL = 'INSERT INTO numeros_asignados (fecha, telefono, observaciones) VALUES (?, ?, ?)';
        $paramSQL = array($num[0], $num[1], $num[2]);

        //$this->_DB->genQuery("SET AUTOCOMMIT = 0");
        $result = $this->_DB->genQuery($sPeticionSQL, $paramSQL);

        if (!$result) {
            $this->errMsg = $this->_DB->errMsg;
            $this->_DB->genQuery("ROLLBACK");
            $this->_DB->genQuery("SET AUTOCOMMIT = 1");
            return false;
        }

        return $result;
    }



    function cantidadNumero($param){

        // Verificar que el numeroe referenciado existe
        if($param!=NULL){
            $arrParam = array($param);
            $tupla = $this->_DB->getFirstRowQuery(
                'SELECT COUNT(*) FROM numeros_asignados WHERE telefono = ?',FALSE,$arrParam);
        }else{
            $tupla = $this->_DB->getFirstRowQuery(
                'SELECT COUNT(*) FROM numeros_asignados',FALSE,NULL);
        }

        return $tupla[0];
    }





    function editNumero($num)
    {
        if (!is_array($num) || count($num) < 3) {
            $this->errMsg = 'Invalid numero data';
            return FALSE;
        }

        // Verificar que el numeroe referenciado existe
        $tupla = $this->_DB->getFirstRowQuery(
            'SELECT COUNT(*) FROM numeros_asignados WHERE telefono = ?',
            FALSE, array($num[1]));
        if ($tupla[0] <= 0) {
            $this->errMsg = _tr('Numero not found');
            return FALSE;
        }

        $sPeticionSQL = 'UPDATE numeros_asignados SET fecha = ?, telefono  = ?, observaciones = ? WHERE telefono = ?';
        $paramSQL = array($num[0], $num[1], $num[2], $num[1]);

        $result = $this->_DB->genQuery($sPeticionSQL, $paramSQL);
        if (!$result) {
            $this->errMsg = $this->_DB->errMsg;
            $this->_DB->genQuery("ROLLBACK");
            $this->_DB->genQuery("SET AUTOCOMMIT = 1");
            return false;
        }

        return $result;
    }

    function deleteAgent($id_agent)
    {
        if (!preg_match('/^[[:digit:]]+$/', $id_agent)) {
            $this->errMsg = '(internal) Invalid agent information';
            return FALSE;
        }

        // BORRAR EN BASE DE DATOS

        $sPeticionSQL = "UPDATE agent SET estatus='I' WHERE number=$id_agent";

        $this->_DB->genQuery("SET AUTOCOMMIT = 0");
        $result = $this->_DB->genQuery($sPeticionSQL);
        if (!$result) {
            $this->errMsg = $this->_DB->errMsg;
            $this->_DB->genQuery("ROLLBACK");
            $this->_DB->genQuery("SET AUTOCOMMIT = 1");
            return false;
        }

        $resp = $this->deleteAgentFile($id_agent);
        if ($resp) {
            $this->_DB->genQuery("COMMIT");
        } else {
            $this->_DB->genQuery("ROLLBACK");
        }
        $this->_DB->genQuery("SET AUTOCOMMIT = 1");

        return $resp;
    }

}
?>
