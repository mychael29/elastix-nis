<?php
/* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 0.5                                                  |
  | http://www.elastix.com                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
  $Id: paloSantoCallsEntrante.class.php,v 1.1.1.1 2018/08/06 21:31:55 Maycol mmeza@newip.pe Exp $ */

include_once("libs/paloSantoDB.class.php");

class paloSantoCallsEntrante
{
    private $_DB;   // Conexión a la base de datos
    var $errMsg;    // Último mensaje de error

    function paloSantoCallsEntrante(&$pDB)
    {
        // Se recibe como parámetro una referencia a una conexión paloDB
        if (is_object($pDB)) {
            $this->_DB =& $pDB;
            $this->errMsg = $this->_DB->errMsg;
        } else {
            $dsn = (string)$pDB;
            $this->_DB = new paloDB($dsn);
            if (!$this->_DB->connStatus) {
                $this->errMsg = $this->_DB->errMsg;
                // debo llenar alguna variable de error
            } else {
                // debo llenar alguna variable de error
            }
        }
    }

    function getReportForm($limit, $offset, $id_agent, $date_start, $date_end)
    {
        $sFechaInicio = isset($date_start) ? $this->_checkDateTimeFormat($date_start) : NULL;
        $sFechaFinal  = isset($date_end) ? $this->_checkDateTimeFormat($date_end) : NULL;
        if (!(is_null($sFechaInicio) || is_null($sFechaFinal)) && $sFechaFinal < $sFechaInicio) {
            $t = $sFechaFinal; $sFechaFinal = $sFechaInicio; $sFechaInicio = $t;
        }
	      $where    = "";
       
        if(!empty($id_agent) & $id_agent !="ALL"){
            $where    .= "agente='$id_agent' AND ";
        }
        if (!is_null($sFechaInicio)) {
            $where   .= "fecha >= '$sFechaInicio' AND ";
        }
        if (!is_null($sFechaFinal)) {
          $sFechaFinal= date('Y-m-d H:i:s', strtotime("$sFechaFinal + 1 day"));
          $where   .= "fecha <= '$sFechaFinal' AND ";
        }
        if(strlen($where)>0) $where = " where  ".trim($where,' AND ');
        $query   = "SELECT campania_id,fecha,nombre_cliente,phone,agente,data FROM formulario_data_entry $where order by fecha desc LIMIT $limit OFFSET $offset";

        //$query = "SELECT form_data_recolected_entry.id,fecha,numero_id,agent,data, campaign_id FROM form_data_recolected_entry 
        //INNER JOIN agent on agent.number=form_data_recolected_entry.agent $where order by fecha desc LIMIT $limit OFFSET $offset";

        $result=$this->_DB->fetchTable($query, FALSE);
        if($result==FALSE){
            $this->errMsg = $this->_DB->errMsg;
            return array();
        }

        return $result;
    }

    function contarDetalleFormEntrantes()
    {
        $query   = "SELECT count(*) FROM formulario_data_entry";
        $result=$this->_DB->fetchTable($query, FALSE);
        if($result==FALSE){
            $this->errMsg = $this->_DB->errMsg;
            return array();
        }
        return $result;
    }

    /**
     * Procedimiento para obtener los agentes de CallCenter. A diferencia del
     * método en modules/agents/Agentes.class.php, este método lista también los
     * agentes inactivos, junto con su estado.
     *
     * @return  mixed   NULL en caso de error, o lista de agentes
     */
    function getAgents(){

        $recordset = $this->_DB->fetchTable(
            'SELECT id, number, name, estatus FROM agent ORDER BY estatus, number',
            TRUE);
        if (!is_array($recordset)) {
            $this->errMsg = '(internal) Failed to fetch agents - '.$this->_DB->errMsg;
            $recordset = NULL;
        }
        return $recordset;
    }

    /**
     * Procedimiento para leer la lista de campañas del CallCenter. Las campañas
     * se listan primero las activas, luego inactivas, luego terminadas, y luego
     * por fecha de creación descendiente.
     *
     * @param unknown $type
     */
    function getCampaigns($type){

        $recordset = $this->_DB->fetchTable(
            'SELECT id, name, estatus '.
            'FROM '.(($type == 'incoming') ? 'campaign_entry' : 'campaign').' '.
            'ORDER BY estatus, datetime_init DESC',
            TRUE);
        if (!is_array($recordset)) {
            $this->errMsg = '(internal) Failed to fetch campaigns - '.$this->_DB->errMsg;
            $recordset = NULL;
        }
        return $recordset;
    }
}
?>
